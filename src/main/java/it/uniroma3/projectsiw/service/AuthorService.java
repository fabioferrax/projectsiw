package it.uniroma3.projectsiw.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.repository.AuthorCrudRepositoryJPA;

public class AuthorService implements BaseService<Author> {
	

	public AuthorService() {

	}
	public Author save(Author author){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		AuthorCrudRepositoryJPA rep = new AuthorCrudRepositoryJPA(em);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		author=rep.save(author);
		tx.commit();
		return author;
	}
	public List<Author> findAll() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		AuthorCrudRepositoryJPA rep =new AuthorCrudRepositoryJPA(em);
		tx.begin();

		List<Author> authors=rep.findAll();
		tx.commit();

		return authors;
	}
	public Author findByKey(int id) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		AuthorCrudRepositoryJPA rep =new AuthorCrudRepositoryJPA(em);
		tx.begin();
		Author author = rep.findByKey(id);
		tx.commit();
		return author;
	}
	public void remove(Author author){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		AuthorCrudRepositoryJPA rep =new AuthorCrudRepositoryJPA(em);
		tx.begin();
		rep.delete(author);
		tx.commit();	
	}
	@Override
	public int removeAll() {
		// TODO Auto-generated method stub
		return 0;
	}
}
