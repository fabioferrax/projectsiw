var app = angular.module("app");


app.controller("operaController", function($scope,$rootScope,$location,callreq,mainServices,imageService,$window,$routeParams){

	$rootScope.showBar = true;
	var baseUrl = mainServices.getBaseUrl();
	$scope.artistOpera = {};
	$rootScope.selectHome = false;
	$rootScope.selectAuthor = false;
	$rootScope.selectFavorite = false;
	$rootScope.selectProfile = false;
	$scope.idOpera = Number($routeParams.id);

	if(localStorage.currentUser) {
		$scope.currentUser = JSON.parse(localStorage.currentUser);

	} else {
		location.href = "#!/login"
	}

	$scope.currentOpera = function(){

		callreq.callReqPost(baseUrl+"operaDetails",{id: $scope.idOpera},
				function (data)
				{	
			$scope.detailsOperas = data.opera;
			$scope.init($scope.detailsOperas.author.id);

			if($scope.detailsOperas.imageUrl == ""){
				$scope.detailsOperas.imageUrl  = "css/image/placeholder-opera0.png";
			}


				}, 
				function (error)
				{
					console.log(error);
				}); 

	}

	$scope.init = function(id){



		callreq.callReqPost(baseUrl+"findAllByAuthor",{ id :id},
				function (data)
				{	

			$scope.artistOpera = data.operas;

			for(var i = 0; i<$scope.artistOpera.length; i++){

				var obj = $scope.artistOpera[i];
				if(obj.imageUrl == ""){
					obj.imageUrl  = "css/image/placeholder-opera0.png";
				}

				if(obj.id == $scope.idOpera){
					$scope.artistOpera.splice(i,1);
					i--
				}
			}


				}, 
				function (error)
				{
					console.log(error);
				});

	}


	$scope.goToDeleteOpera = function(id){
		$scope.deletePopupShow = true;
		$scope.idDeleteOpera = id;
	}

	$scope.goToModifyOpera = function(opera){
		$scope.popupShow = true
		$scope.modelUpdateOpera = JSON.parse(JSON.stringify(opera));
	}
	$scope.chooseOpera = function(idOpera){
		$scope.popupChooseShow = true
		$scope.idOperaImage = idOpera;

	}

	$scope.operaComments = function(){

		callreq.callReqPost(baseUrl+"findOperaComment",{ id : $scope.idOpera},
				function (data)
				{	

			$scope.comments = data.comments;

				}, 
				function (error)
				{
					console.log(error);
				});
	}

	$scope.checkKeypress = function(event,idOpera,modelComment){

		if(event.charCode == 13) {

			event.preventDefault();
			$scope.idOpera = idOpera;
			$scope.modelComment = modelComment;
			$scope.sendMsg();
		}
	}

	$scope.sendMsg = function(){

		var json = {
				idOpera: $scope.idOpera,
				comment: $scope.modelComment,
				idUtente: $scope.currentUser.id	
		}

		callreq.callReqPost(baseUrl+"insertComment",json,
				function (data)
				{	

			$scope.operaComments($scope.idOpera);
			$scope.modelComment = null;

				}, 
				function (error)
				{
					console.log(error);
				});

	};


	$scope.close = function(){
		$scope.popupShow = false;
		$scope.popupChooseShow = false;
		$scope.deletePopupShow = false;
		$scope.popupShowImage = false;

	}

	$scope.deleteOpera = function(){

		callreq.callReqPost(baseUrl+"deleteOpera ",{id: $scope.idDeleteOpera},
				function (data)
				{	

			location.href="#!/slideshow"

				}, 
				function (error)
				{
					console.log(error);
				});
	}

	$scope.modifyOpera = function(modelUpdateOpera){

		var json = {
				title: modelUpdateOpera.title,
				dimension: {
					width: modelUpdateOpera.dimension.width,
					height: modelUpdateOpera.dimension.height
				},
				year: modelUpdateOpera.year,
				technique : modelUpdateOpera.technique,
				id: modelUpdateOpera.id,
				idAuthor : modelUpdateOpera.author.id,
				description: modelUpdateOpera.description
		}
		callreq.callReqPost(baseUrl+"updateOpera",json,
				function (data)
				{	

			$scope.detailsOperas = data.opera;
			$scope.popupShow = false;

				}, 
				function (error)
				{
					console.log(error);
				});
	}

	$scope.operaFindById = function(id){
		location.href="#!/operaDetails&id="+id;
	}

	$scope.chooseService = function(){

		$scope.detailsOperas.imageUrl = imageService.imageUpdate($scope.idOperaImage,1);
		$scope.popupChooseShow = false;
	}


	$scope.preview = function(imgUrl){
		$scope.popupShowImage = true;
		$scope.image = imgUrl;

	}

	$scope.rating = function(num){

		callreq.callReqPost(baseUrl+"addRating ",{id: $scope.idOpera, rating: Number(num)},
				function (data)
				{	

			$scope.detailsOperas.avarageRating = Number(data.rating);


				}, 
				function (error)
				{
					console.log(error);
				});
	}



});