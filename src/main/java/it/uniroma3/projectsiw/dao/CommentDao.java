package it.uniroma3.projectsiw.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.uniroma3.projectsiw.helper.HibernateHelper;
import it.uniroma3.projectsiw.model.Comment;
import it.uniroma3.projectsiw.model.Opera;

public class CommentDao implements BasicDao<Comment, Integer>{
 
	@SuppressWarnings("finally")
	public Integer insert(Comment model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Integer id = 0;
		try{
			id = (Integer) session.save(model);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return id;
	}

	public boolean delete(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			String hql = "delete Comment where id =:i";
			Query query = session.createQuery(hql);
			query.setParameter("i", id);
			result = query.executeUpdate() == 1;
		}catch(Exception e){
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public List<Comment> findAll() {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		List<Comment> comment = new ArrayList<Comment>();
		try{
			String hql = "from Comment";
			Query query = session.createQuery(hql); 
			comment = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return comment;
	}

	public boolean update(Comment model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			String hql = "update Comment set comment =: c, date =:d where id =: i";
			Query query = session.createQuery(hql);
			query.setParameter("c", model.getComment());
			query.setParameter("d", model.getDate());
			query.setParameter("i", model.getId());
			result = query.executeUpdate() == 1;
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public Comment findById(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Comment comment = null;
		try{
			String hql = "from Comment where id = :id";
			Query query = session.createQuery(hql); 
			query.setParameter("id", id);
			comment = (Comment) query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return comment;
	}

	public List<Comment> findByOpera(int id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		List<Comment> comment = new ArrayList<Comment>();
		try{
			String hql = "from Comment where opera_id=:i order by date desc";
			Query query = session.createQuery(hql); 
			query.setParameter("i", id);
			comment = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return comment;
	}

}
