package it.uniroma3.projectsiw.helper;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import it.uniroma3.projectsiw.helper.View.ManagedOpera;
import it.uniroma3.projectsiw.model.Opera;

public class Mapper {
	private static ObjectMapper mapper = new ObjectMapper();
	
	
	@SuppressWarnings("rawtypes")
	public static String toJson(Object responseModel, Class view){
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		String result = "";
		try {
			result = mapper
					.writerWithView(view)
					.writeValueAsString(responseModel);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public static String appendJsonList(String firstJson, String jsonAppended, String listName){
		String result = firstJson.substring(0, firstJson.length()-1)+", \""+listName+"\": "+jsonAppended.substring(0,jsonAppended.length())+"}";
		return result;
	}
	public static <T> JSONArray toJSONArray(JSONArray responseModel, Class view) {
		// TODO Auto-generated method stub
		return new JSONArray(toJson(responseModel, view));
	}
	public static JSONObject toJSONObject(Object responseModel, Class view) {
		// TODO Auto-generated method stub
		return new JSONObject(toJson(responseModel, view));
	}
}
