app.service('callreq', function ($http) {



	this.callReqPost = function(requrl,reqdata, succb, errcb){


		$http({
			method: "POST",
			url: requrl,
			data: reqdata
		}).then(function (msgResponse)
				{
			
			if(msgResponse.data.status == 'ERROR'){
				toastr.error(msgResponse.data.message);
			}
			succb(msgResponse.data.jsonData);
			
			
				}, function (error)
				{

			

					errcb(error);
				});
	};

	this.callReqGet = function(requrl,reqdata, succb, errcb){

		$http({
			method: "GET",
			url: requrl,
			params: reqdata
		}).then(function (msgResponse)
				{

			succb(msgResponse.data);

				}, function (error)
				{
					errcb(error);
				});
	};
});