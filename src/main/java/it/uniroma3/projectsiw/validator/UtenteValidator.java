package it.uniroma3.projectsiw.validator;

import it.uniroma3.projectsiw.request.UserRequest;
import it.uniroma3.projectsiw.response.JSONResponse;

public class UtenteValidator{
	
	public static boolean isValid(UserRequest ur, JSONResponse response){
		boolean result = true;
		if(ur.getUsername()==null || ur.getUsername().equals("")){
			response.addError("userUsername", "Username obbligatorio");
			result = false;
		}
		if(ur.getPassword()==null || ur.getPassword().equals("")){
			response.addError("userPassword", "Password obbligatoria");
			result = false;
		}else{
			if(ur.getConfirmPassword()==null || ur.getConfirmPassword().equals("")){
				response.addError("userConfirmPassword", "Conferma la password");
				result = false;
			}else{
				if(!ur.getPassword().equals(ur.getConfirmPassword())){
					response.addError("userConfirmPassword", "Le password non coincidono");
					result = false;
				}
			}
		}
		
		
		return result;
	}
	public static boolean isValid(String pass, String confirmPass){
		boolean result = true;
		
		if(pass==null || pass.equals("")){
			result = false;
		}else{
			if(confirmPass==null || confirmPass.equals("")){
				result = false;
			}else{
				if(!pass.equals(confirmPass)){
					result = false;
				}
			}
		}		
		return result;
	}

}
