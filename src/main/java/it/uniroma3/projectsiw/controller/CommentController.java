package it.uniroma3.projectsiw.controller;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.uniroma3.projectsiw.dao.CommentDao;
import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.model.Comment;
import it.uniroma3.projectsiw.model.Opera;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.request.CommentRequest;
import it.uniroma3.projectsiw.request.OperaRequest;
import it.uniroma3.projectsiw.response.JSONResponse;

import java.util.*;


@RestController
public class CommentController implements BasicController<CommentRequest> {

	private CommentDao dao = new CommentDao();

	@RequestMapping(path = "/insertComment" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String insert(@RequestBody CommentRequest ar){
		JSONResponse response = new JSONResponse();
		if(ar.getComment().length()<255){
			Comment comment = new Comment();
			comment.setComment(ar.getComment());
			comment.setDate(new Date());
			comment.setOpera(Opera.findById(ar.getIdOpera()));
			comment.setUtente(Utente.findById(ar.getIdUtente()));
			comment.setId(dao.insert(comment));
			response.addData("comment", comment.toJson(View.ManagedComment.class));
			response.setStatus(ResponseStatus.SUCCESS);
		}else{
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage("Commento troppo lungo");
		}
		return response.toString();
	}

	@RequestMapping(path = "/deleteComment" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody CommentRequest ar){
		JSONResponse response = new JSONResponse();
		if(dao.delete(ar.getId())){
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Commento cancellato");
		}else{
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Errore durante la cancellazione");
		}
		return response.toString();
	}

	@RequestMapping(path = "/updateComment" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody CommentRequest ar){
		JSONResponse response = new JSONResponse();
		Comment comment = new Comment();
		comment.setComment(ar.getComment());
		comment.setDate(new Date());
		comment.setId(ar.getId());
		if(dao.update(comment)){
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Commento modificato");
		}else{
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Errore durante la modifica");
		}
		return response.toString();
	}

	@RequestMapping(path = "/findOperaComment" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String findOperaComment(@RequestBody OperaRequest or){
		JSONResponse response = new JSONResponse();
		System.out.println(or.getId());
		response.addData("comments", Comment.toJsonArray(dao.findByOpera(or.getId()), View.ManagedComment.class));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();
	}
	@Override
	public String findAll() {
		// TODO Auto-generated method stub
		return null;
	}



}
