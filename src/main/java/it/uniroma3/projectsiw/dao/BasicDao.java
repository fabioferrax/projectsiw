package it.uniroma3.projectsiw.dao;

import java.util.List;

public interface BasicDao <T,I>{
	public Integer insert(T model);
	public boolean delete(I id);
	public List<T> findAll();
	public boolean update(T model);
	public T findById(I id);
}
