var app = angular.module("app");

app.controller('profileController',function($scope, $rootScope,$location,callreq,mainServices,imageService,$window){


	//Variable initialization
	$scope.modelUser = {};

	//status bar
	$rootScope.showBar = true;
	$rootScope.selectHome = false;
	$rootScope.selectAuthor = false;
	$rootScope.selectProfile = true;
	$rootScope.favoriteOperas = {};

	var baseUrl = mainServices.getBaseUrl();

	if(localStorage.currentUser) {
		$scope.currentUser = JSON.parse(localStorage.currentUser);

	} else {
		location.href = "#!/login"
	}


	$scope.userFavorite = function(id){

		callreq.callReqPost(baseUrl+"favorite",{id: id},
				function (data)
				{	
			$rootScope.favoriteOperas = data.favorite;

				}, 
				function (error)
				{
					console.log(error);
				}); 

	}
	$scope.modifyUsername = function(modelUpdateUser){
		callreq.callReqPost(baseUrl+"updateUsername",modelUpdateUser,
				function (data)
				{	
			$scope.currentUser.username = data.username;
			localStorage.currentUser = JSON.stringify($scope.currentUser);
			$scope.popupShow = false;
				}, 
				function (error)
				{
					console.log(error);
				}); 

	}
	$scope.modifyPassword = function(modelUpdateUser){

		modelUpdateUser.id = $scope.currentUser.id;
		callreq.callReqPost(baseUrl+"updatePassword",modelUpdateUser,
				function (data)
				{	

			$scope.popupShowPassword = false;
				}, 
				function (error)
				{
					console.log(error);
				}); 

	}
	$scope.close = function(){
		$scope.popupShow = false;
		$scope.popupChooseShow = false;
		$scope.popupShowPassword = false;
	}
	$scope.goToModifyUsername = function(user){
		$scope.popupShow = true
		$scope.modelUpdateUser = JSON.parse(JSON.stringify(user));
	}
	$scope.goToModifyPassword = function(userId){
		$scope.popupShowPassword = true
	}

	$scope.chooseService = function(){
		$scope.currentUser = JSON.parse(localStorage.currentUser)
		$scope.currentUser.imageUrl = imageService.imageUpdate($scope.currentUser.id,2);
		location.href="#!/profile";
		$scope.popupChooseShow = false;
	}
	$scope.chooseUser = function(){
		$scope.popupChooseShow = true
	}

	$scope.remove = function(id){


		var json = {
				idOpera  : id,
				idUtente : $scope.currentUser.id
		};

		callreq.callReqPost(baseUrl+"removeFromFavorite", json,
				function (data)
				{	
					for(var i = 0; i < $rootScope.favoriteOperas.length; i++){
						var obj = $rootScope.favoriteOperas[i];
						if(obj.id == id){
							$rootScope.favoriteOperas.splice(i,1);
						}
					}
				}, 
				function (error)
				{
					console.log(error);
				});
	}

	$scope.goToOpera = function(id){
		location.href="#!/operaDetails&id="+id
	}
});