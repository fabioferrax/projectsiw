var app = angular.module("app");


app.controller("artistController", function($scope,$rootScope,$location,callreq,mainServices,$window,toastr){

	$rootScope.showBar = true;
	var baseUrl = mainServices.getBaseUrl();
	$scope.modelAuthors = {};
	$scope.detailsAuthor = {};
	$rootScope.selectHome = false;
	$rootScope.selectAuthor = true;
	$rootScope.selectProfile = false;
	
	if(localStorage.currentUser) {
		$scope.currentUser = JSON.parse(localStorage.currentUser);

	} else {
		location.href = "#!/login"
	}


	$scope.initAuthor = function(){

		callreq.callReqPost(baseUrl+"/authorFindAll", null,
				function (data)
				{	

			$scope.modelAuthors = data.authors;
			for(var i = 0; i<$scope.modelAuthors.length; i++){
				var obj = $scope.modelAuthors[i];
				if(obj.imageUrl == ""){
					obj.imageUrl  = "css/image/placeholder-artist0.png";
				}
			}

				}, 
				function (error)
				{
					console.log(error);
				});

	}


	$scope.authorDetails = function(id){
		$window.location.href = '#!/authorDetails&id='+id;
	}

	$scope.goToAddAuthor = function(){
		$scope.popupShowInsert = true;
	}


	$scope.close = function(){
		$scope.popupShowInsert = false;
	}

	$scope.addAuthor = function(modelInsertAuthor){
		
		if(modelInsertAuthor == null | modelInsertAuthor == undefined ){
			toastr.error('Compilare i campi');
		}else if( modelInsertAuthor.name == null || modelInsertAuthor.name == undefined | modelInsertAuthor.name==""){
			toastr.error('Compilare il campo nome');
		} else if(modelInsertAuthor.surname == null || modelInsertAuthor.surname == undefined | modelInsertAuthor.surname==""){
			toastr.error('Compilare il campo cognome');
		}  else if(modelInsertAuthor.birthday == null || modelInsertAuthor.birthday == undefined | modelInsertAuthor.birthday==""){
			toastr.error('Compilare il campo data di nascita');
		}  else if(modelInsertAuthor.nationality == null || modelInsertAuthor.nationality == undefined | modelInsertAuthor.nationality==""){
			toastr.error('Compilare il campo nazionalità');
		}  else if(modelInsertAuthor.biography == null || modelInsertAuthor.biography == undefined | modelInsertAuthor.biography==""){
			toastr.error('Compilare il campo biografia');
		} else{
			var json = {

					name: modelInsertAuthor.name, 
					surname: modelInsertAuthor.surname, 
					birthday : modelInsertAuthor.birthday, 
					deathDate: modelInsertAuthor.deathDate, 
					nationality: modelInsertAuthor.nationality,
					biography: modelInsertAuthor.biography
			}
			callreq.callReqPost(baseUrl+"insertAuthor", json,
					function (data)
					{	
				$scope.modelAuthors.push(data.author);
				$scope.popupShowInsert = false;
					}, 
					function (error)
					{
						console.log(error);
					});
		}
	}
});