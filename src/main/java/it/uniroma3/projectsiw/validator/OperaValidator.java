package it.uniroma3.projectsiw.validator;

import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.request.OperaRequest;
import it.uniroma3.projectsiw.response.JSONResponse;

public class OperaValidator {

	
	public static boolean isValid(OperaRequest or, JSONResponse response){
		boolean result = true;
		
		if(or.getTitle() == null || or.getTitle().equals("")){
			response.addError("operaTitle", "Titolo obbligatorio");
			result = false;
		}
		if(or.getYear() == 0){
			response.addError("operaYear", "Anno obbligatorio");
			result = false;
		}
		if(or.getIdAuthor() == 0){
			response.addError("operaAuthor", "Autore non valido");
			result = false;
		}else{
			if(Author.findById(or.getIdAuthor())==null){
				response.addError("operaAuthor", "Autore non valido");
				result = false;
			}
		}
			
		
		return result;
	}
}
