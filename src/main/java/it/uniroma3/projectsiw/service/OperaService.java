package it.uniroma3.projectsiw.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import it.uniroma3.projectsiw.model.Opera;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.repository.OperaCrudRepositoryJPA;

public class OperaService  implements BaseService<Opera>{
	

	public OperaService() {

	}
	public Opera save(Opera opera){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		OperaCrudRepositoryJPA rep = new OperaCrudRepositoryJPA(em);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		opera=rep.save(opera);
		tx.commit();
		return opera;
	}
	public List<Opera> findAll() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		OperaCrudRepositoryJPA rep =new OperaCrudRepositoryJPA(em);
		tx.begin();

		List<Opera> operas =rep.findAll();
		tx.commit();

		return operas;
	}
	public Opera findByKey(int id) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		OperaCrudRepositoryJPA rep =new OperaCrudRepositoryJPA(em);
		tx.begin();
		Opera opera = rep.findByKey(id);
		tx.commit();
		return opera;
	}
	public void remove(Opera opera){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		OperaCrudRepositoryJPA rep =new OperaCrudRepositoryJPA(em);
		tx.begin();
		rep.delete(opera);
		tx.commit();	
	}

	@Override
	public int removeAll() {
		// TODO Auto-generated method stub
		return 0;
	}
}
