package it.uniroma3.projectsiw.controller;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.uniroma3.projectsiw.dao.AuthorDao;
import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.request.AuthorRequest;
import it.uniroma3.projectsiw.response.JSONResponse;
import it.uniroma3.projectsiw.validator.AuthorValidator;

@RestController
public class AuthorController implements BasicController<AuthorRequest> {

	private AuthorDao dao = new AuthorDao();

	@RequestMapping(path = "/insertAuthor" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String insert(@RequestBody AuthorRequest ar){

		JSONResponse response = new JSONResponse();
		if(AuthorValidator.isValid(ar, response)){
			Author author = new Author();
			author.setName(ar.getName());
			author.setSurname(ar.getSurname());
			author.setBhirtday(ar.getBirthday());
			author.setDeathDate(ar.getDeathDate());
			author.setBiography(ar.getBiography());
			author.setNationality(ar.getNationality());
			author.setId(dao.insert(author));
			response.addData("author",author.toJson(View.ManagedAuthor.class));
			response.setStatus(ResponseStatus.SUCCESS);
		}else{
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();
	}
	@RequestMapping(path = "/deleteAuthor" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody AuthorRequest ar) {
		JSONResponse response = new JSONResponse();

		if(dao.delete(ar.getId())){
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Autore eliminato");
		}else{
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Errore durante l'eliminazione");
		}
		return response.toString();
	}

	@RequestMapping(path = "/updateAuthor" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody AuthorRequest ar) {
		JSONResponse response = new JSONResponse();
		if(AuthorValidator.isValid(ar, response)){
			Author author = Author.findById(ar.getId());
			author.setName(ar.getName());
			author.setNationality(ar.getNationality());
			author.setSurname(ar.getSurname());
			author.setBhirtday(ar.getBirthday());
			author.setBiography(ar.getBiography());
			author.setDeathDate(ar.getDeathDate());
			if(dao.update(author)){
				response.addData("author", author.toJson(View.ManagedAuthor.class));
				response.setStatus(ResponseStatus.SUCCESS);
				response.setMessage("Autore modificato");
			}else{
				response.setStatus(ResponseStatus.ERROR);
				response.setMessage("Errore durante la modificata");
			}
		}

		return response.toString();
	}

	@RequestMapping(path = "/authorDetails" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String authorDetails(@RequestBody AuthorRequest ar){

		JSONResponse response = new JSONResponse();
		response.addData("author", Author.findById(ar.getId()).toJson(View.ManagedAuthor.class));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();
	}

	@RequestMapping(path = "/authorFindAll" , produces = "application/json;charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String findAll(){

		JSONResponse response = new JSONResponse();
		List<Author> authors = Author.findAll();
		response.addData("authors", Author.toJsonArray(authors, View.ManagedList.class));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();
	}




}
