package it.uniroma3.projectsiw.controller.jpa;
import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import it.uniroma3.projectsiw.dao.UserDao;
import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.helper.Role;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.model.Opera;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.request.UserRequest;
import it.uniroma3.projectsiw.response.JSONResponse;
import it.uniroma3.projectsiw.service.UserService;
import it.uniroma3.projectsiw.validator.UtenteValidator;


@RestController
public class UserControllerJpa implements BasicController<UserRequest>{

	private UserDao dao = new UserDao();
	private UserService userService = new UserService();
 
	@RequestMapping(path = "/registerJpa", produces = "application/json; charset=utf-8" , method = RequestMethod.POST )
	@ResponseBody
	public String insert(@RequestBody UserRequest ur){

		JSONResponse response = new JSONResponse();
		if(UtenteValidator.isValid(ur, response)){
			Utente utente = new Utente();
			utente.setUsername(ur.getUsername());	
			utente.setRole(Role.USER);
			utente.setPassword(ur.getPassword().hashCode()+"");
			//utente.setId(dao.insert(utente));
			utente = userService.save(utente);
			response.addData("user",utente.toJson(View.ManagedUser.class));
			response.setStatus(ResponseStatus.SUCCESS);
		} else {
			response.setStatus(ResponseStatus.ERROR);
		}

		return response.toString();
	}
	@RequestMapping(path = "/loginJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String login(@RequestBody UserRequest ur){
		JSONResponse response = new JSONResponse();
		Utente utente = userService.findToLogin(ur.getUsername(), ur.getPassword().hashCode()+"");
		System.out.println(utente.getUsername());
		if(utente!=null){
			response.addData("user",utente.toJson(View.ManagedUser.class));
			response.setStatus(ResponseStatus.SUCCESS);
		}else{
			response.setMessage("Username o password errati");
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();
	}
	@RequestMapping(path = "/favoriteJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String favorite(@RequestBody UserRequest ur){
		JSONResponse response = new JSONResponse();
		Utente utente = dao.findById(ur.getId());
		response.addData("favorite",Opera.toJsonArray(View.ManagedOperaFavorite.class, utente.getFavoriteWorks()));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();
	}
	@RequestMapping(path = "/profileJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String profile(@RequestBody UserRequest ur){
		JSONResponse response = new JSONResponse();
		try{
			Utente utente = dao.findById(ur.getId());
			response.addData("user",utente.toJson(View.ManagedUser.class));
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(NullPointerException ne){
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage("Utente inesistente");
		}catch(Exception e){
			e.printStackTrace();
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();
	}

	@Override
	public String delete(@RequestBody UserRequest ar) {
		// TODO Auto-generated method stub
		return null;
	}
	@RequestMapping(path = "/updateUsernameJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody UserRequest ur) {
		JSONResponse response  = new JSONResponse();
		Utente utente = Utente.findById(ur.getId());
		utente.setUsername(ur.getUsername());
		if(dao.update(utente)){
			response.addData("username", utente.getUsername());
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Username modificato");
		}else{
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Impossile modificare l'username");
		}

		return response.toString();
	}
	@RequestMapping(path = "/updatePasswordJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String updatePassword(@RequestBody UserRequest ur) {
		JSONResponse response  = new JSONResponse();
		Utente utente = Utente.findById(ur.getId());
		if(utente.getPassword().equals(ur.getOldPassword().hashCode()+"")){
			if(UtenteValidator.isValid(ur.getPassword(), ur.getConfirmPassword())){
				utente.setPassword(ur.getPassword().hashCode()+"");
				if(dao.update(utente)){
					response.addData("user", utente.toJson(View.ManagedUser.class));
					response.setStatus(ResponseStatus.SUCCESS);
					response.setMessage("Password modificato");
				}else{
					response.setStatus(ResponseStatus.ERROR);
					response.setMessage("Impossile modificare la password");
				}
			}else{
				response.setStatus(ResponseStatus.WARNING);
				response.setMessage("Le password non coincidono");
			}
		}else{
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage("Vecchia password errata");
		}
		return response.toString();
	}
	@Override
	public String findAll() {
		// TODO Auto-generated method stub
		return null;
	}
}
