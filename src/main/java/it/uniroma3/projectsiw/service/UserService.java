package it.uniroma3.projectsiw.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.repository.UserCrudRepositoryJPA;

public class UserService implements BaseService<Utente>{
	

	public UserService() {

	}
	public Utente save(Utente utente){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		UserCrudRepositoryJPA rep = new UserCrudRepositoryJPA(em);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		utente=rep.save(utente);
		tx.commit();
		return utente;
	}
	public List<Utente> findAll() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		UserCrudRepositoryJPA rep =new UserCrudRepositoryJPA(em);
		tx.begin();

		List<Utente> utenti=rep.findAll();
		tx.commit();

		return utenti;
	}
	public Utente findByKey(int id) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		UserCrudRepositoryJPA rep =new UserCrudRepositoryJPA(em);
		tx.begin();
		Utente utente = rep.findByKey(id);
		tx.commit();
		return utente;
	}
	public void remove(Utente utente){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		UserCrudRepositoryJPA rep =new UserCrudRepositoryJPA(em);
		tx.begin();
		rep.delete(utente);
		tx.commit();	
	}

	@Override
	public int removeAll() {
		// TODO Auto-generated method stub
		return 0;
	}
	public Utente findToLogin(String username, String password) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		UserCrudRepositoryJPA rep =new UserCrudRepositoryJPA(em);
		tx.begin();
		Utente utente = rep.findToLogin(username, password);
		tx.commit();	
		return utente;
	}
}
