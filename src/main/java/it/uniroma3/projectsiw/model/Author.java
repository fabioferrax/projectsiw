package it.uniroma3.projectsiw.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import org.json.JSONArray;
import org.json.JSONObject;

import it.uniroma3.projectsiw.dao.AuthorDao;
import it.uniroma3.projectsiw.helper.Constants;
import it.uniroma3.projectsiw.helper.Mapper;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.helper.View.ManagedAuthor;

@Entity
public class Author {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String name;
	@Column
	private String biography;
	
	@Column
	@Lob
	@JsonIgnore
	private byte[] image;
	
	@Column
	private String surname;
	
	@Column
	private String nationality;
	
	@Column
	private Date bhirtday;
	
	@Column
	private Date deathDate;
	
	//Not show if view isn't managed author
	
	@OneToMany(mappedBy = "author",cascade = CascadeType.ALL)
	@JsonView(View.ManagedAuthor.class)
	private List<Opera> works;
	
	private static AuthorDao dao = new AuthorDao();


	public Author(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getBhirtday() {
		return bhirtday;
	}

	public void setBhirtday(Date bhirtday) {
		this.bhirtday = bhirtday;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}
	
	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public List<Opera> getWorks() {
		return works;
	}

	public void setWorks(List<Opera> works) {
		this.works = works;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public JSONObject toJsonFull(){
		JSONObject json = new JSONObject();
		json.put("id", this.getId());
		json.put("name", this.getName());
		json.put("surname", this.getSurname());
		json.put("birthday", this.getBhirtday());
		json.put("death", this.getDeathDate());
		json.put("nationality", this.getNationality());
		json.put("works", this.getWorks());
		return json;
	}
	public JSONObject toJsonBase(){
		JSONObject json = new JSONObject();
		json.put("id", this.getId());
		json.put("name", this.getName());
		json.put("surname", this.getSurname());
		json.put("birthday", this.getBhirtday());
		json.put("death", this.getDeathDate());
		json.put("nationality", this.getNationality());
		return json;
	}
	public JSONObject toJsonList(){
		JSONObject json = new JSONObject();
		json.put("id", this.getId());
		json.put("name", this.getName());
		json.put("surname", this.getSurname());
		return json;
	}
	public static JSONArray toJsonArrayFull(List<Author> authors){
		JSONArray arr = new JSONArray();
		
		for(Author author : authors){
			
			arr.put(author.toJsonFull());
		}
		
		return arr;
	}
	public static JSONArray toJsonArrayList(List<Author> authors){
		JSONArray arr = new JSONArray();
		
		for(Author author : authors){
			
			arr.put(author.toJsonList());
		}
		
		return arr;
	}

	public static Author findById(Integer idAuthor) {
		
		return dao.findById(idAuthor);
	}
	
	public static List<Author> findAll() {
		
		return dao.findAll();
	}
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getImageUrl(){
		if(this.getImage()!=null && this.getImage().length != 0)
			return Constants.pathMedia+"?id="+this.getId()+"&model=3&dummy="+(new Date().getTime());
		else
			return "";
	}
	//return json object from author mapped by view
	public JSONObject toJson(Class view) {
		return new JSONObject(Mapper.toJson(this, view));
	}
	
	//return json object from author mapped by view
	public static JSONArray toJsonArray(List<Author> arr,Class view) {		
		return new JSONArray(Mapper.toJson(arr, view));
	}

	public static List<Opera> findAllOperas(Integer id) {
		// TODO Auto-generated method stub
		return dao.findAllOperas(id);
	}
	public static void update(Author author) {
		dao.update(author);
	}
}
