package it.uniroma3.projectsiw.helper;

//model json view
public class View {
	
	public static class ManagedList{}
	public static class ManagedAuthor{}
	public static class ManagedComment{}
	public static class ManagedOperaFavorite{}
	public static class ManagedOpera extends ManagedOperaFavorite{}
	public static class ManagedUser{}

}
