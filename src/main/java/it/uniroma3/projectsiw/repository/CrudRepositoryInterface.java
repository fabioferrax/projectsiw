package it.uniroma3.projectsiw.repository;

import java.util.List;

import it.uniroma3.projectsiw.model.Utente;

public interface CrudRepositoryInterface<T> {
	public T save(T model);
	public T findByKey(int id);
	public List<T> findAll();
	public void delete(T model);
	public int deleteAll();
}
