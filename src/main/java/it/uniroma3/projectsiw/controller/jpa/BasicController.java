package it.uniroma3.projectsiw.controller.jpa;


import org.springframework.web.bind.annotation.RequestBody;


public interface BasicController <T>{

	public String insert(@RequestBody T ar);
	public String delete(@RequestBody T ar);
	public String update(@RequestBody T ar);
	public String findAll();
}
