package it.uniroma3.projectsiw.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.uniroma3.projectsiw.helper.HibernateHelper;
import it.uniroma3.projectsiw.model.Utente;

public class UserCrudRepositoryJPA implements CrudRepositoryInterface<Utente>{
	private EntityManager em;
	private EntityTransaction tx;
	
	public UserCrudRepositoryJPA(EntityManager em) {
		this.em=em;
	}
	@Override
	public Utente save(Utente model) {
		if(model.getId()==null)
			em.persist(model);
		else
			return  em.merge(model);  //update cos� l'oggetto di ritorno � managed
		return model;
	}

	@Override
	public Utente findByKey(int id) {
		return em.find(Utente.class, id);
	}

	@Override
	public List<Utente> findAll() {
		Query query = em.createQuery("from Utente");
		return query.getResultList();
	}

	@Override
	public void delete(Utente model) {
		em.remove(model);
	}

	@Override
	public int deleteAll() {
		Query query = em.createQuery("delete from Utente");
		return query.executeUpdate();
	}
	public Utente findToLogin(String username, String password) {
		Query query = em.createQuery("select u from Utente u where username LIKE :u AND password LIKE :p");
		query.setParameter("u", username);
		query.setParameter("p", password);
		return (Utente) query.getSingleResult();
	}

}
