package it.uniroma3.projectsiw.helper;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateHelper {

	private static final SessionFactory sessionFactory = getSessionFactory();

	public static SessionFactory getSessionFactory() {
		try {
			if (sessionFactory == null) {
				// loads configuration and mappings
				Configuration configuration = new Configuration().configure();
				configuration.configure("hibernate.cfg.xml");

				return configuration.buildSessionFactory();
			}
			return sessionFactory;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Session startTransaction() {
		try {
			return getSessionFactory().openSession();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}

