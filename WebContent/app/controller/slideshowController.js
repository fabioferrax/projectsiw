var app = angular.module("app");


app.controller("slideshowController", function($scope,$rootScope,$location,callreq,mainServices,operaServices,$window){
	//Variable initialization
	$rootScope.showBar = true;
	var baseUrl = mainServices.getBaseUrl();
	$scope.modelOpera = {};
	$rootScope.selectHome = true;
	$rootScope.selectAuthor = false;
	$rootScope.selectFavorite = false;
	$rootScope.selectProfile = false;



	if(localStorage.currentUser) {
		$scope.currentUser = JSON.parse(localStorage.currentUser);

	} else {
		location.href = "#!/login"
	}


	$scope.init = function(){

		callreq.callReqPost(baseUrl+"operaFindAll", {id: $scope.currentUser.id},
				function (data)
				{	

			$scope.modelOpera = data.operas;

			for(var i = 0; i<$scope.modelOpera.length; i++){

				var obj = $scope.modelOpera[i];
				if(obj.imageUrl == ""){
					obj.imageUrl  = "css/image/placeholder-opera0.png";
				}

			}

				}, 
				function (error)
				{
					console.log(error);
				});

	}
	

	

	$scope.operaDetails = function(id){
		
	
		
		$window.location.href = '#!/operaDetails'+"&id="+id;

	}

	
	$scope.saveFavorite = function (id){

		var json = {
				idOpera  : id,
				idUtente : $scope.currentUser.id
		};


		callreq.callReqPost(baseUrl+"addToFavorite", json,
				function (data)
				{	
			$scope.init();
				}, 
				function (error)
				{
					console.log(error);
				});
	}

	$scope.remove = function(id){


		var json = {
				idOpera  : id,
				idUtente : $scope.currentUser.id
		};

		callreq.callReqPost(baseUrl+"removeFromFavorite", json,
				function (data)
				{	
			$scope.init();
				}, 
				function (error)
				{
					console.log(error);
				});
	}

	

});