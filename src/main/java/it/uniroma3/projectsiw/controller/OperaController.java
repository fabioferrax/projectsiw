package it.uniroma3.projectsiw.controller;

import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.SimpleFormatter;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.projectsiw.dao.OperaDao;
import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.model.Opera;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.request.AuthorRequest;
import it.uniroma3.projectsiw.request.FavoriteRequest;
import it.uniroma3.projectsiw.request.OperaRequest;
import it.uniroma3.projectsiw.request.UserRequest;
import it.uniroma3.projectsiw.response.JSONResponse;
import it.uniroma3.projectsiw.validator.OperaValidator;


@RestController
public class OperaController implements  BasicController<OperaRequest> {

	private OperaDao dao = new OperaDao();

	@RequestMapping(path = "/insertOpera" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String insert(@RequestBody OperaRequest or){

		JSONResponse response = new JSONResponse();

		if(OperaValidator.isValid(or, response)){
			Opera opera = new Opera();
			opera.setTitle(or.getTitle());
			opera.setTechnique(or.getTechnique());
			opera.setYear(or.getYear());
			opera.setDescription(or.getDescription());
			opera.setDimension(or.getDimension());
			opera.setAuthor(Author.findById(or.getIdAuthor()));
			opera.setId(dao.insert(opera));
			response.addData("opera",opera.toJson(View.ManagedOpera.class));
			response.setStatus(ResponseStatus.SUCCESS);
		}
		else{
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();
	}
	@RequestMapping(path = "/deleteOpera" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody OperaRequest ar) {
		JSONResponse response  = new JSONResponse();
		if(dao.delete(ar.getId())){
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Opera cancellata");
		}else{
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Errore durante la cancellazione");
		}
		return response.toString();
	}
	@RequestMapping(path = "/updateOpera" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody OperaRequest or) {
		JSONResponse response = new JSONResponse();
		if(OperaValidator.isValid(or, response)){
			Opera opera = Opera.findById(or.getId());
			opera.setTitle(or.getTitle());
			opera.setTechnique(or.getTechnique());
			opera.setYear(or.getYear());
			opera.setDescription(or.getDescription());
			opera.setDimension(or.getDimension());
			opera.setAuthor(Author.findById(or.getIdAuthor()));
			if(dao.update(opera)){
				response.addData("opera", opera.toJson(View.ManagedOpera.class));
				response.setStatus(ResponseStatus.SUCCESS);
				response.setMessage("Opera modificata");
			}else{
				response.setStatus(ResponseStatus.ERROR);
				response.setMessage("Impossile modificare l'opera");
			}
		}
		return response.toString();
	}

	@RequestMapping(path = "/operaFindAll" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String findAll(@RequestBody UserRequest ur){

		Utente utente = Utente.findById(ur.getId());
		JSONResponse response = new JSONResponse();
		List<Opera> operas = Opera.findAll();
		for(Opera opera: operas){
			if(utente.getFavoriteWorks().contains(opera))
				opera.setFavorite(true);
		}
		response.addData("operas",Opera.toJsonArray(View.ManagedOpera.class, operas));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();

	}
	@RequestMapping(path = "/findAllByAuthor" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String findAllByAuthor(@RequestBody AuthorRequest ar){

		JSONResponse response = new JSONResponse();
		List<Opera> operas = Author.findAllOperas(ar.getId());
		response.addData("operas",Opera.toJsonArray(View.ManagedOpera.class, operas));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();

	}
	@RequestMapping(path = "/operaDetails" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String operaDetails(@RequestBody OperaRequest or){

		JSONResponse response = new JSONResponse();
		Opera opera = dao.findById(or.getId());
		response.addData("opera",opera.toJson(View.ManagedOpera.class));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();
	}
	@RequestMapping(path = "/addToFavorite" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String addToFavorite(@RequestBody FavoriteRequest or){
		JSONResponse response = new JSONResponse();
		try{
			Opera opera = dao.findById(or.getIdOpera());
			Utente utente = Utente.findById(or.getIdUtente());
			utente.addFavorite(opera);
			opera.addFavoriteUtente(utente);
			dao.update(opera);
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();

	}
	@RequestMapping(path = "/removeFromFavorite" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String removeFromFavorite(@RequestBody FavoriteRequest or){
		JSONResponse response = new JSONResponse();
		try{
			Opera opera = dao.findById(or.getIdOpera());
			Utente utente = Utente.findById(or.getIdUtente());
			utente.getFavoriteWorks().remove(opera);
			opera.getUtentiFavourits().remove(utente);
			dao.update(opera);
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();

	}
	@RequestMapping(path = "/addRating" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String addRating(@RequestBody OperaRequest or){
		JSONResponse response = new JSONResponse();
		try{
			Opera opera = dao.findById(or.getId());
			opera.setRating(opera.getRating()+or.getRating());
			opera.setRatingCount(opera.getRatingCount()+1);
			DecimalFormat df = new DecimalFormat("#.#");
			dao.update(opera);
			response.addData("rating", opera.getAvarageRating());
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();

	}
	@Override
	public String findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
