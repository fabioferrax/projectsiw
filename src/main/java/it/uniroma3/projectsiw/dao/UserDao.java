package it.uniroma3.projectsiw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.uniroma3.projectsiw.helper.HibernateHelper;
import it.uniroma3.projectsiw.model.Opera;
import it.uniroma3.projectsiw.model.Utente;

public class UserDao implements BasicDao<Utente, Integer>{
 
	@SuppressWarnings("finally")
	public Integer insert(Utente model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Integer id = 0;
		try{
			id = (Integer) session.save(model);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return id;
	}

	public boolean delete(Integer id){
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			String hql = "delete Utente where id =:i";
			Query query = session.createQuery(hql);
			query.setParameter("i", id);
			result = query.executeUpdate() == 1;
		}catch(Exception e){
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public List<Utente> findAll() {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		List<Utente> users = new ArrayList<Utente>();
		try{
			String hql = "from Utente";
			Query query = session.createQuery(hql); 
			users = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return users;
	}

	public boolean update(Utente model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			session.update(model);
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public Utente findById(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Utente user = null;
		try{
			String hql = "from Utente where id = :id";
			Query query = session.createQuery(hql); 
			query.setParameter("id", id);
			user = (Utente) query.getSingleResult();
		}catch(Exception e){
		}finally {
			tx.commit();
			session.close();
		}
		return user;
	}

	public Utente findToLogin(String username, String password) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Utente user = null;
		try{
			String hql = "from Utente where username LIKE :u AND password LIKE :p";
			Query query = session.createQuery(hql); 
			query.setParameter("u", username);
			query.setParameter("p", password);
			user = (Utente) query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return user;
	}
	public Utente findByUsername(String username) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Utente user = null;
		try{
			String hql = "from Utente where username LIKE :u ";
			Query query = session.createQuery(hql); 
			query.setParameter("u", username);
			user = (Utente) query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return user;
	}
	

}
