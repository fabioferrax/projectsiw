package it.uniroma3.projectsiw.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.imageio.ImageWriter;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.connector.OutputBuffer;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.model.Opera;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.response.JSONResponse;

@RestController
public class ImageController {
	 
	@RequestMapping(path = "/addImage" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String addOperaImage(@RequestPart("image") MultipartFile image, HttpServletRequest request){
		JSONResponse response = new JSONResponse();
		try{
			int id = Integer.parseInt(request.getParameter("id"));
			int model = Integer.parseInt(request.getParameter("model"));
			if(!image.isEmpty())
			switch(model){
			case 1:
				Opera opera = Opera.findById(id);
				opera.setImage(image.getBytes());
				Opera.update(opera);
				response.addData("image", opera.getImageUrl());
				break;
			case 2:
				Utente utente = Utente.findById(id);
				utente.setImage(image.getBytes());
				Utente.update(utente);
				response.addData("image", utente.getImageUrl());
				break;
			case 3:
				Author author = Author.findById(id);
				author.setImage(image.getBytes());
				Author.update(author);
				response.addData("image",author.getImageUrl());
				break;
			}
			
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();
	}
	@RequestMapping(path = "/getImage" )
	@ResponseBody
	public void getImage(HttpServletRequest request, HttpServletResponse response){
		
		try{
			int id = Integer.parseInt(request.getParameter("id"));
			int model = Integer.parseInt(request.getParameter("model"));
			byte[] image = null;
			switch(model){
			case 1:
				Opera opera = Opera.findById(id);
				image = opera.getImage();
				break;
			case 2:
				Utente utente = Utente.findById(id);
				image = utente.getImage();
				break;
			case 3:
				Author author = Author.findById(id);
				image = author.getImage();
				break;
			}
			response.setContentType("image/jpg");
			try {
				if (image!=null)
					response.getOutputStream().write(image);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}
	
}
