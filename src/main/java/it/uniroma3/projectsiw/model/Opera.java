package it.uniroma3.projectsiw.model;

import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import it.uniroma3.projectsiw.dao.OperaDao;
import it.uniroma3.projectsiw.helper.Constants;
import it.uniroma3.projectsiw.helper.Mapper;
import it.uniroma3.projectsiw.helper.View;

@Entity
public class Opera {

	@Column
	private String title;

	@Column(length = 500)
	private String description;

	@Column
	private int year;

	@Column
	private Dimension dimension;

	@ManyToOne
	@JsonView(View.ManagedOpera.class)
	private Author author;

	@Column
	private String technique;
	@Column(columnDefinition = "int default 0")
	private Integer ratingCount = 0;

	@JoinTable(name = "utente_opere",
			joinColumns = @JoinColumn(name = "opera_id",
			nullable = false,
			updatable = false),
			inverseJoinColumns = @JoinColumn(name = "utente_id",
			nullable = false,
			updatable = false))
	@ManyToMany(cascade =
{
		CascadeType.DETACH,
		CascadeType.MERGE,
		CascadeType.REFRESH,
		CascadeType.PERSIST
},fetch = FetchType.LAZY, targetEntity = Utente.class)
	@JsonIgnore
	private List<Utente> utentiFavourits;
	@Transient
	private boolean favorite;
	@JsonIgnore
	@Column(columnDefinition = "float default 0")
	private float rating = 0;

	@Column
	@Lob
	@JsonIgnore
	private byte[] image;

	@OneToMany(mappedBy = "opera")
	@JsonIgnore
	private List<Comment> comments;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private static OperaDao dao = new OperaDao();

	public Opera(){

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public String getTechnique() {
		return technique;
	}

	public void setTechnique(String technique) {
		this.technique = technique;
	}

	public Integer getRatingCount() {
		return ratingCount;
	}

	public void setRatingCount(Integer ratingCount) {
		this.ratingCount = ratingCount;
	}
	public double getAvarageRating() {
		if(ratingCount>0)return roundToHalf(rating/ratingCount);
		else return 0.0;
	}
	public static double roundToHalf(float d) {
	    return Math.round(d * 2) / 2.0;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public static OperaDao getDao() {
		return dao;
	}

	public static void setDao(OperaDao dao) {
		Opera.dao = dao;
	}

	public String getImageUrl(){
		if(this.getImage()!=null && this.getImage().length != 0)
			return Constants.pathMedia+"?id="+this.getId()+"&model=1&dummy="+(new Date().getTime());
		else
			return "";
	}

	public JSONObject toJson(Class view) {
		// TODO Auto-generated method stub
		return new JSONObject(Mapper.toJson(this, view));
	}
	public static JSONArray toJsonArray(Class view,List<Opera> operas){
		return new JSONArray(Mapper.toJson(operas, view));
	}

	public static Opera findById(Integer id){
		return dao.findById(id);
	}

	public static List<Opera> findAll(){
		return dao.findAll();
	}

	public List<Utente> getUtentiFavourits() {
		if(utentiFavourits == null) utentiFavourits = new ArrayList<Utente>();
		return utentiFavourits;
	}

	public void setUtentiFavourits(List<Utente> utentiFavourits) {
		this.utentiFavourits = utentiFavourits;
	}

	public JSONObject toJsonFull(){
		JSONObject json = new JSONObject();
		json.put("id", this.getId());
		json.put("title", this.getTitle());
		json.put("year", this.getYear());
		json.put("author", this.getAuthor().toJsonList());
		json.put("dimension", this.getDimension());
		json.put("rating", this.getRatingCount());
		return json;
	}
	//	public JSONObject toJsonBase(){
	//		JSONObject json = new JSONObject();
	//		json.put("id", this.getId());
	//		json.put("name", this.getName());
	//		json.put("surname", this.getSurname());
	//		json.put("birthday", this.getBhirtday());
	//		json.put("death", this.getDeathDate());
	//		json.put("nationality", this.getNationality());
	//		return json;
	//	}
	//	public JSONObject toJsonList(){
	//		JSONObject json = new JSONObject();
	//		json.put("id", this.getId());
	//		json.put("name", this.getName());
	//		json.put("surname", this.getSurname());
	//		return json;
	//	}

	public static JSONArray toJsonArrayFull(List<Opera> operas){
		JSONArray arr = new JSONArray();

		for(Opera opera : operas){

			arr.put(opera.toJsonFull());
		}

		return arr;
	}

	public void addComment(Comment comment) {
		if(this.comments == null) this.comments = new ArrayList<Comment>();
		this.comments.add(comment);
	}

	public void addFavoriteUtente(Utente utente) {
		if(utentiFavourits == null) utentiFavourits = new ArrayList<Utente>();
		utentiFavourits.add(utente);

	}

	public static void update(Opera opera) {
		dao.update(opera);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Opera other = (Opera) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setFavorite(boolean b) {
		this.favorite = b;
	}

	public boolean isFavorite() {
		return favorite;
	}

	public static void deleteByAuthor(Integer id) {
		dao.deleteByAuthor(id);
	}


}
