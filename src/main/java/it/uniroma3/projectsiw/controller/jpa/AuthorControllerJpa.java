package it.uniroma3.projectsiw.controller.jpa;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.uniroma3.projectsiw.dao.AuthorDao;
import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.request.AuthorRequest;
import it.uniroma3.projectsiw.response.JSONResponse;
import it.uniroma3.projectsiw.service.AuthorService;
import it.uniroma3.projectsiw.validator.AuthorValidator;

@RestController
public class AuthorControllerJpa implements BasicController<AuthorRequest> {

	private AuthorService authorService = new AuthorService();

	@RequestMapping(path = "/insertAuthorJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String insert(@RequestBody AuthorRequest ar){

		JSONResponse response = new JSONResponse();
		if(AuthorValidator.isValid(ar, response)){
			Author author = new Author();
			author.setName(ar.getName());
			author.setSurname(ar.getSurname());
			author.setBhirtday(ar.getBirthday());
			author.setDeathDate(ar.getDeathDate());
			author.setNationality(ar.getNationality());
			author = authorService.save(author);
			response.addData("author",author.toJson(View.ManagedAuthor.class));
			response.setStatus(ResponseStatus.SUCCESS);
		}else{
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();
	}
	@RequestMapping(path = "/deleteAuthorJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody AuthorRequest ar) {
		JSONResponse response = new JSONResponse();
		Author author = authorService.findByKey(ar.getId());
		authorService.remove(author);
		response.setStatus(ResponseStatus.SUCCESS);
		response.setMessage("Autore eliminato");

		return response.toString();
	}

	@RequestMapping(path = "/updateAuthorJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody AuthorRequest ar) {
		JSONResponse response = new JSONResponse();
		if(AuthorValidator.isValid(ar, response)){
			try{
				Author author = Author.findById(ar.getId());
				author.setName(ar.getName());
				author.setNationality(ar.getNationality());
				author.setSurname(ar.getSurname());
				author.setBhirtday(ar.getBirthday());
				author.setDeathDate(ar.getDeathDate());
				author = authorService.save(author);
				response.addData("author", author.toJson(View.ManagedAuthor.class));
				response.setStatus(ResponseStatus.SUCCESS);
				response.setMessage("Autore modificato");
			}catch(Exception e){
				response.setStatus(ResponseStatus.ERROR);
				response.setMessage("Errore durante la modificata");
			}
		}

		return response.toString();
	}

	@RequestMapping(path = "/authorDetailsJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String authorDetails(@RequestBody AuthorRequest ar){

		JSONResponse response = new JSONResponse();
		response.addData("author", authorService.findByKey(ar.getId()).toJson(View.ManagedAuthor.class));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();
	}

	@RequestMapping(path = "/authorFindAllJpa" , produces = "application/json;charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String findAll(){
		
		JSONResponse response = new JSONResponse();
		List<Author> authors = authorService.findAll();
		response.addData("authors", Author.toJsonArray(authors, View.ManagedList.class));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();

	}




}
