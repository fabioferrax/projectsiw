package it.uniroma3.projectsiw.controller.jpa;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.projectsiw.dao.OperaDao;
import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.model.Opera;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.request.AuthorRequest;
import it.uniroma3.projectsiw.request.FavoriteRequest;
import it.uniroma3.projectsiw.request.OperaRequest;
import it.uniroma3.projectsiw.request.UserRequest;
import it.uniroma3.projectsiw.response.JSONResponse;
import it.uniroma3.projectsiw.service.OperaService;
import it.uniroma3.projectsiw.service.UserService;
import it.uniroma3.projectsiw.validator.OperaValidator;


@RestController
public class OperaControllerJpa implements  BasicController<OperaRequest> {

	private OperaService operaService = new OperaService();

	@RequestMapping(path = "/insertOperaJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String insert(@RequestBody OperaRequest or){

		JSONResponse response = new JSONResponse();

		if(OperaValidator.isValid(or, response)){
			Opera opera = new Opera();
			opera.setTitle(or.getTitle());
			opera.setTechnique(or.getTechnique());
			opera.setYear(or.getYear());
			opera.setDescription(or.getDescription());
			opera.setDimension(or.getDimension());
			opera.setAuthor(Author.findById(or.getIdAuthor()));
			opera = operaService.save(opera);
			response.addData("opera",opera.toJson(View.ManagedOpera.class));
			response.setStatus(ResponseStatus.SUCCESS);
		}
		else{
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();
	}
	@RequestMapping(path = "/deleteOperaJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody OperaRequest ar) {
		JSONResponse response  = new JSONResponse();
		try{
			Opera opera = operaService.findByKey(ar.getId());
			operaService.remove(opera);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("Opera cancellata");
		}catch(Exception e){
			response.setStatus(ResponseStatus.ERROR);
			response.setMessage("Errore durante la cancellazione");
		}
		return response.toString();
	}
	@RequestMapping(path = "/updateOperaJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody OperaRequest or) {
		JSONResponse response = new JSONResponse();
		if(OperaValidator.isValid(or, response)){
			try{
				Opera opera = Opera.findById(or.getId());
				opera.setTitle(or.getTitle());
				opera.setTechnique(or.getTechnique());
				opera.setYear(or.getYear());
				opera.setDescription(or.getDescription());
				opera.setDimension(or.getDimension());
				opera.setAuthor(Author.findById(or.getIdAuthor()));
				opera = operaService.save(opera);
				response.addData("opera", opera.toJson(View.ManagedOpera.class));
				response.setStatus(ResponseStatus.SUCCESS);
				response.setMessage("Opera modificata");
			}catch(Exception e){
				response.setStatus(ResponseStatus.ERROR);
				response.setMessage("Impossile modificare l'opera");
			}
		}
		return response.toString();
	}

	@RequestMapping(path = "/operaFindAllJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String findAll(@RequestBody UserRequest ur){

		Utente utente = Utente.findById(ur.getId());
		JSONResponse response = new JSONResponse();
		List<Opera> operas = Opera.findAll();
		for(Opera opera: operas){
			if(utente.getFavoriteWorks().contains(opera))
				opera.setFavorite(true);
		}
		response.addData("operas",Opera.toJsonArray(View.ManagedOpera.class, operas));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();

	}
	@RequestMapping(path = "/findAllByAuthorJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String findAllByAuthor(@RequestBody AuthorRequest ar){

		JSONResponse response = new JSONResponse();
		List<Opera> operas = Author.findAllOperas(ar.getId());
		response.addData("operas",Opera.toJsonArray(View.ManagedOpera.class, operas));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();

	}
	@RequestMapping(path = "/operaDetailsJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String operaDetails(@RequestBody OperaRequest or){

		JSONResponse response = new JSONResponse();
		Opera opera = operaService.findByKey(or.getId());
		response.addData("opera",opera.toJson(View.ManagedOpera.class));
		response.setStatus(ResponseStatus.SUCCESS);
		return response.toString();
	}
	@RequestMapping(path = "/addToFavoriteJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String addToFavorite(@RequestBody FavoriteRequest or){
		JSONResponse response = new JSONResponse();
		try{
			Opera opera = operaService.findByKey(or.getIdOpera());
			UserService us = new UserService();
			Utente utente = us.findByKey(or.getIdUtente());
			utente.addFavorite(opera);
			opera.addFavoriteUtente(utente);
			operaService.save(opera);
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();

	}
	@RequestMapping(path = "/removeFromFavoriteJpa" , produces = "application/json; charset=utf-8" , method = RequestMethod.POST)
	@ResponseBody
	public String removeFromFavorite(@RequestBody FavoriteRequest or){
		JSONResponse response = new JSONResponse();
		try{
			Opera opera = operaService.findByKey(or.getIdOpera());
			UserService us = new UserService();
			Utente utente = us.findByKey(or.getIdUtente());
			utente.getFavoriteWorks().remove(opera);
			opera.getUtentiFavourits().remove(utente);
			operaService.save(opera);
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			response.setStatus(ResponseStatus.ERROR);
		}
		return response.toString();

	}
	@Override
	public String findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
