package it.uniroma3.projectsiw.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.eclipse.jdt.internal.compiler.ast.FakedTrackingVariable;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;

import it.uniroma3.projectsiw.dao.UserDao;
import it.uniroma3.projectsiw.helper.Constants;
import it.uniroma3.projectsiw.helper.Mapper;
import it.uniroma3.projectsiw.helper.Role;
import it.uniroma3.projectsiw.helper.View;

@Entity
public class Utente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(unique = true)
	private String username;
	
	@JsonIgnore
	@Column
	private String password;
	@Column
	@Lob
	@JsonIgnore
	private byte[] image;
	@Column
	@Enumerated
	private Role role;
	@JoinTable(name = "utente_opere",
			joinColumns = @JoinColumn(name = "utente_id",
			nullable = false,
			updatable = false),
			inverseJoinColumns = @JoinColumn(name = "opera_id",
			nullable = false,
			updatable = false))
	@ManyToMany(cascade =
{
		CascadeType.DETACH,
		CascadeType.MERGE,
		CascadeType.REFRESH,
		CascadeType.PERSIST
},fetch = FetchType.LAZY, targetEntity = Opera.class)
	@JsonIgnore
	private List<Opera> favoriteWorks;
	
	@JsonIgnore
	@OneToMany(mappedBy = "utente")
	private List<Comment> comments;
	
	private static UserDao dao = new UserDao();
	
	public Utente(){
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Opera> getFavoriteWorks() {
		if(favoriteWorks == null) favoriteWorks = new ArrayList<Opera>();
		return favoriteWorks;
	} 

	public void setFavoriteWorks(List<Opera> favoriteWorks) {
		this.favoriteWorks = favoriteWorks;
	}
	
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public JSONObject toJsonFull(){
		JSONObject json = new JSONObject();
		json.put("id", this.getId());
		json.put("username", this.getUsername());
		json.put("password", this.getPassword());
		json.put("favoriteWorks", this.getFavoriteWorks());
		return json;
	}
	public JSONObject toJsonBase(){
		JSONObject json = new JSONObject();
		json.put("id", this.getId());
		json.put("role", this.getRole());
		json.put("username", this.getUsername());
		json.put("password", this.getPassword());
		return json;
	}
	public JSONObject toJson(Class view) {
		// TODO Auto-generated method stub
		return new JSONObject(Mapper.toJson(this, view));
	}
	public static JSONArray toJsonArray(Class view,List<Utente> utenti){
		return new JSONArray(Mapper.toJson(utenti, view));
	}
	public static Utente findById(Integer id){
		return dao.findById(id);
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getImageUrl(){
		if(this.getImage()!=null && this.getImage().length != 0)
			return Constants.pathMedia+"?id="+this.getId()+"&model=2&dummy="+(new Date().getTime());
		else
			return "";
	}
	public void addFavorite(Opera opera) {
		if(favoriteWorks == null) favoriteWorks = new ArrayList<Opera>();
		favoriteWorks.add(opera);
	}
	public static void update(Utente utente) {
		dao.update(utente);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Utente other = (Utente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
