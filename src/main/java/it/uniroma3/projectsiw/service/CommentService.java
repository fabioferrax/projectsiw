package it.uniroma3.projectsiw.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import it.uniroma3.projectsiw.model.Comment;
import it.uniroma3.projectsiw.model.Utente;
import it.uniroma3.projectsiw.repository.CommentCrudRepositoryJPA;

public class CommentService  implements BaseService<Comment>{
	

	public CommentService() {

	}
	public Comment save(Comment comment){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		CommentCrudRepositoryJPA rep = new CommentCrudRepositoryJPA(em);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		comment=rep.save(comment);
		tx.commit();
		return comment;
	}
	public List<Comment> findAll() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		CommentCrudRepositoryJPA rep =new CommentCrudRepositoryJPA(em);
		tx.begin();

		List<Comment> comments=rep.findAll();
		tx.commit();

		return comments;
	}
	public Comment findByKey(int id) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		CommentCrudRepositoryJPA rep =new CommentCrudRepositoryJPA(em);
		tx.begin();
		Comment comment = rep.findByKey(id);
		tx.commit();
		return comment;
	}
	public void remove(Comment comment){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		CommentCrudRepositoryJPA rep =new CommentCrudRepositoryJPA(em);
		tx.begin();
		rep.delete(comment);
		tx.commit();	
	}
	@Override
	public int removeAll() {
		// TODO Auto-generated method stub
		return 0;
	}
	public List<Comment> findByOpera(int id) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("gallery-unit");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		CommentCrudRepositoryJPA rep =new CommentCrudRepositoryJPA(em);
		tx.begin();
		List<Comment> comments = rep.findByOpera(id);
		tx.commit();	
		return comments;
	}
}
