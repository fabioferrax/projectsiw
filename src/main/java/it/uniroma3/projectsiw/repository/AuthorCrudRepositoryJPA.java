package it.uniroma3.projectsiw.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import it.uniroma3.projectsiw.model.Author;

public class AuthorCrudRepositoryJPA implements CrudRepositoryInterface<Author>{
	private EntityManager em;
	private EntityTransaction tx;
	
	public AuthorCrudRepositoryJPA(EntityManager em) {
		this.em=em;
	}
	@Override
	public Author save(Author model) {
		if(model.getId()==null)
			em.persist(model);
		else
			return  em.merge(model);  //update cos� l'oggetto di ritorno � managed
		return model;
	}

	@Override
	public Author findByKey(int id) {
		return em.find(Author.class, id);
	}

	@Override
	public List<Author> findAll() {
		Query query = em.createQuery("from Author");
		return query.getResultList();
	}

	@Override
	public void delete(Author model) {
		em.remove(model);
	}

	@Override
	public int deleteAll() {
		Query query = em.createQuery("delete from Author");
		return query.executeUpdate();
	}

}
