package it.uniroma3.projectsiw.response;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import it.uniroma3.projectsiw.helper.ResponseStatus;
import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.model.Opera;

public class JSONResponse extends JSONObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JSONResponse(){
		this.put("jsonData", new JSONObject());
		this.getJSONObject("jsonData").put("error", new JSONObject());
		this.getJSONObject("jsonData").put("message", "");
		this.getJSONObject("jsonData").put("status", ResponseStatus.ERROR);
	}
	public void setMessage(String msg){
		this.getJSONObject("jsonData").put("message", msg);
	}
	public void setStatus(ResponseStatus status){
		this.getJSONObject("jsonData").put("status", status);
	}
	public void addData(String name,JSONObject json){
		this.getJSONObject("jsonData").put(name, json);
	}
	public<T> void addData(String name,List<T> json){
		JSONArray arr = new JSONArray();
		Opera a = null;
		for(T value : json){
			a = (Opera)value;
			arr.put(a.toJsonFull());
		}
		this.getJSONObject("jsonData").put(name, arr);
	}
	public void addData(String name, String data){
		this.getJSONObject("jsonData").put(name, data);
	}
	public void addData(String name, Object data){
		this.getJSONObject("jsonData").put(name, data);
	}
	public void addData(String name, JSONArray array){
		this.getJSONObject("jsonData").put(name, array);
	}
	public void addError(String name, String error){
		this.getJSONObject("jsonData").getJSONObject("error").put(name, error);
	}
}
