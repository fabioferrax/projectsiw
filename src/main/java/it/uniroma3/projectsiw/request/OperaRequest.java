package it.uniroma3.projectsiw.request;



import java.awt.Dimension;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

import it.uniroma3.projectsiw.helper.Role;
import it.uniroma3.projectsiw.model.Author;

public class OperaRequest {

	private String title;
	private int year;
	private Dimension dimension;
	private int height;
	private int width;
	private Integer idAuthor;
	private String technique;
	private Integer ratingCount;
	private float rating;
	private int id;
	private String description;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Dimension getDimension() {
		return dimension;
	}
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}
	public Integer getIdAuthor() {
		return idAuthor;
	}
	public void setIdAuthor(Integer idAuthor) {
		this.idAuthor = idAuthor;
	}
	public String getTechnique() {
		return technique;
	}
	public void setTechnique(String technique) {
		this.technique = technique;
	}
	public Integer getRatingCount() {
		return ratingCount;
	}
	public void setRatingCount(Integer ratingCount) {
		this.ratingCount = ratingCount;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
