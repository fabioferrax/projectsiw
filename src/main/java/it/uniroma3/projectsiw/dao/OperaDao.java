package it.uniroma3.projectsiw.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.uniroma3.projectsiw.helper.HibernateHelper;
import it.uniroma3.projectsiw.model.Opera;

public class OperaDao implements BasicDao<Opera, Integer> {
 
	@SuppressWarnings("finally")
	public Integer insert(Opera model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Integer id = 0;
		try{
			id = (Integer) session.save(model);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return id;
	}

	public boolean delete(Integer id){
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			String hql = "delete Opera where id =:i";
			Query query = session.createQuery(hql);
			query.setParameter("i", id);
			result = query.executeUpdate() == 1;
		}catch(Exception e){
			throw e;
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public List<Opera> findAll() {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		List<Opera> opera = new ArrayList<Opera>();
		try{
			String hql = "from Opera order by id";
			Query query = session.createQuery(hql); 
			opera = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return opera;
	}

	public boolean update(Opera model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			session.update(model);
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public Opera findById(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Opera opera = null;
		try{
			String hql = "from Opera where id = :id";
			Query query = session.createQuery(hql); 
			query.setParameter("id", id);
			opera = (Opera) query.getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return opera;
	}
	public List<Opera> findByAuthorId(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		List<Opera> opera = new ArrayList<Opera>();
		try{
			String hql = "from Opera where author_id =:id";
			Query query = session.createQuery(hql); 
			query.setParameter("id", id);
			opera = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return opera;
	}

	public void deleteByAuthor(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			String hql = "delete Opera where author_id =:i";
			Query query = session.createQuery(hql);
			query.setParameter("i", id);
			result = query.executeUpdate() == 1;
		}catch(Exception e){
			throw e;
		}finally {
			tx.commit();
			session.close();
		}
	}
}
