package it.uniroma3.projectsiw.helper;

import java.io.Serializable;

public enum ResponseStatus  implements Serializable{
	
	ERROR,WARNING,INFO,SUCCESS, REGISTERERROR, USERNAMEALREADYEXIST

}
