package it.uniroma3.projectsiw.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import it.uniroma3.projectsiw.model.Comment;

public class CommentCrudRepositoryJPA implements CrudRepositoryInterface<Comment>{
	private EntityManager em;
	private EntityTransaction tx;
	
	public CommentCrudRepositoryJPA(EntityManager em) {
		this.em=em;
	}
	@Override
	public Comment save(Comment model) {
		if(model.getId()==null)
			em.persist(model);
		else
			return  em.merge(model);  //update cos� l'oggetto di ritorno � managed
		return model;
	}

	@Override
	public Comment findByKey(int id) {
		return em.find(Comment.class, id);
	}

	@Override
	public List<Comment> findAll() {
		Query query = em.createQuery("from Comment");
		return query.getResultList();
	}

	@Override
	public void delete(Comment model) {
		em.remove(model);
	}

	@Override
	public int deleteAll() {
		Query query = em.createQuery("delete from Comment");
		return query.executeUpdate();
	}
	public List<Comment> findByOpera(int id) {
		Query query = em.createQuery("from Comment where opera_id =:id");
		query.setParameter("id", id);
		return query.getResultList();
	}

}
