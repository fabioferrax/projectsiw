package it.uniroma3.projectsiw.request;

public class FavoriteRequest {
	
	private Integer id;
	private Integer idOpera;
	private Integer idUtente;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdOpera() {
		return idOpera;
	}
	public void setIdOpera(Integer idOpera) {
		this.idOpera = idOpera;
	}
	public Integer getIdUtente() {
		return idUtente;
	}
	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}
	
	
	
}
