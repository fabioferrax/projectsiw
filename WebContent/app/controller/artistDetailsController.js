var app = angular.module("app");


app.controller("artistDetailsController", function($scope,$rootScope,$location,callreq,mainServices,imageService,$routeParams,toastr){

	$rootScope.showBar = true;
	var baseUrl = mainServices.getBaseUrl();
	$scope.modelAuthors = {};
	$scope.detailsAuthor = {};
	$rootScope.selectHome = false;
	$rootScope.selectAuthor = false;
	$rootScope.selectProfile = false;
	$scope.idAuthor = Number($routeParams.id);

	if(localStorage.currentUser) {
		$scope.currentUser = JSON.parse(localStorage.currentUser);

	} else {
		location.href = "#!/login"
	}

	$scope.initDetails = function(){



		callreq.callReqPost(baseUrl+"authorDetails", {id: $scope.idAuthor},
				function (data)
				{	
			$scope.artistOpera =data.author;
			if($scope.artistOpera.imageUrl == "")
				$scope.artistOpera.imageUrl = "css/image/placeholder-artist.png"

					for(var i = 0; i<$scope.artistOpera.works.length; i++){

						var obj = $scope.artistOpera.works[i];
						if(obj.imageUrl == ""){
							obj.imageUrl  = "css/image/placeholder-opera0.png";
						}
					}

				}, 
				function (error)
				{
					console.log(error);
				});
	}

	$scope.close = function(){
		$scope.popupShow = false;
		$scope.popupChooseShow = false;
		$scope.deletePopupShow = false;
		$scope.popupShowInsert = false;

	}

	$scope.goToDeleteAuthor = function(id){
		$scope.deletePopupShow = true;
		$scope.idDeleteAuthor = id;
	}

	$scope.goToModifyAuthor = function(author){
		$scope.popupShow = true
		$scope.modelUpdateAuthor = JSON.parse(JSON.stringify(author));
		$scope.modelUpdateAuthor.birthday = new Date(author.bhirtday);
		$scope.modelUpdateAuthor.deathDate = new Date(author.deathDate);
	}
	$scope.chooseAuthor = function(idAuthor){
		$scope.popupChooseShow = true
		$scope.idAuthorImage = idAuthor;
	}

	$scope.modifyAuthor = function(modelUpdateAuthor){

		var json = {
				id: modelUpdateAuthor.id, 
				name: modelUpdateAuthor.name, 
				surname: modelUpdateAuthor.surname, 
				birthday : modelUpdateAuthor.birthday, 
				deathDate: modelUpdateAuthor.deathDate, 
				nationality: modelUpdateAuthor.nationality,
				biography: modelUpdateAuthor.biography
		}
		callreq.callReqPost(baseUrl+"updateAuthor", json,
				function (data)
				{	
			$scope.artistOpera = data.author;
			$scope.popupShow = false;
				}, 
				function (error)
				{
					console.log(error);
				});

	}

	$scope.chooseService = function(){
		$scope.artistOpera.imageUrl = imageService.imageUpdate($scope.idAuthorImage,3);
		$scope.popupChooseShow = false;
	}

	$scope.goToAddOpera = function(idAuthor){
		$scope.popupShowInsert = true;
		$scope.idAuthorAdd = idAuthor;
	}
	$scope.addOpera = function(modelAddpera){

		if(modelAddpera == null | modelAddpera == undefined ){
			toastr.error('Compilare i campi');
		}else if( modelAddpera.title == null || modelAddpera.title == undefined | modelAddpera.title==""){
			toastr.error('Compilare il campo titolo');
		} else if(modelAddpera.year == null || modelAddpera.year == undefined | modelAddpera.year==""){
			toastr.error('Compilare il campo anno');
		}  else if(modelAddpera.technique == null || modelAddpera.technique == undefined | modelAddpera.technique==""){
			toastr.error('Compilare il campo tecnica');
		}  else if(modelAddpera.description == null || modelAddpera.description == undefined | modelAddpera.description==""){
			toastr.error('Compilare il campo descrizione');
		}  else if(modelAddpera.dimension.width == null || modelAddpera.dimension.width == undefined | modelAddpera.dimension.width==""){
			toastr.error('Compilare il campo dimensione');
		}   else if(modelAddpera.dimension.height == null || modelAddpera.dimension.height == undefined | modelAddpera.dimension.height==""){
			toastr.error('Compilare il campo dimensione');
		}else{


			var json = {
					title: modelAddpera.title,
					dimension: {
						width: modelAddpera.dimension.width,
						height: modelAddpera.dimension.height
					},
					year: modelAddpera.year,
					technique : modelAddpera.technique,
					idAuthor : $scope.idAuthorAdd,
					description: modelAddpera.description
			}
			callreq.callReqPost(baseUrl+"insertOpera", json,
					function (data)
					{	
				$scope.artistOpera.works.push(data.opera);
				$scope.popupShowInsert = false;
					}, 
					function (error)
					{
						console.log(error);
					});
		}
	}

	$scope.deleteAuthor = function(){

		callreq.callReqPost(baseUrl+"deleteAuthor ", {id: $scope.idDeleteAuthor},
				function (data)
				{	

			location.href="#!/artists"
				}, 
				function (error)
				{
					console.log(error);
				});


	}


	$scope.goToOpera = function(id){
		location.href="#!/operaDetails&id="+id
	}


});