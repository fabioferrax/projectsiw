var app = angular.module("app");

app.controller('loginController',function($scope, $rootScope,$location,callreq,mainServices,toastr){



	//status bar
	$rootScope.showBar = false;

	//baseUrl from service
	var baseUrl = mainServices.getBaseUrl();

	$scope.login = function(){
		
		if($scope.modelUser == undefined)
			 toastr.error('Compilare i campi');
		else if($scope.modelUser.username == undefined | $scope.modelUser.username == null | $scope.modelUser.username == "")
			toastr.error('Campi errati');
		else if($scope.modelUser.password == undefined | $scope.modelUser.password == null | $scope.modelUser.password == "")
			toastr.error('Campi errati');
		else{

		callreq.callReqPost(baseUrl+"login", $scope.modelUser,
				function (data)
				{	
			
			if(data.status == "ERROR"){
				toastr.error(data.message);
			} else{

			$rootScope.currentUser = data.user;
			if($rootScope.currentUser.imageUrl == ""){
				$rootScope.currentUser.imageUrl  = "css/image/placeholder-artist.png";
			}
			localStorage.currentUser = JSON.stringify($rootScope.currentUser);

			location.href="#!/slideshow"
			}
				}, 
				function (error)
				{
					console.log(error);
				});
		
		}

	}

	$scope.goToRegister = function(){

		location.href = "#!/register"

	}

});