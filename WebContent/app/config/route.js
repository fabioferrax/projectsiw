var app = angular.module("app");

app.config(function ($routeProvider) {

	$routeProvider

	.when("/", {
		templateUrl: "views/login.html",
		controller : "loginController"
	})
	.when("/register", {
		templateUrl: "views/register.html",
		controller : "registerController"
	})
	.when("/artists", {
		templateUrl: "views/artist.html",
		controller : "artistController"
	})
	.when("/slideshow", {
		templateUrl: "views/slideshow.html",
		controller : "slideshowController"
	})
	.when("/operaDetails&id=:id", {
		templateUrl: "views/operaDetails.html",
		controller : "operaController"
	})
	.when("/authorDetails&id=:id", {
		templateUrl: "views/authorDetails.html",
		controller : "artistDetailsController"
	})
		.when("/profile", {
		templateUrl: "views/profile.html",
		controller : "profileController"
	})
	.otherwise({
		redirectTo: "/"
	});  
});