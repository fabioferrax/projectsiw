package it.uniroma3.projectsiw.request;

public class CommentRequest {
	
	private int id;
	private String comment;
	private int idOpera;
	private int idUtente;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getIdOpera() {
		return idOpera;
	}
	public void setIdOpera(int idOpera) {
		this.idOpera = idOpera;
	}
	public int getIdUtente() {
		return idUtente;
	}
	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}
	
	
	
}
