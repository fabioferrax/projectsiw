package it.uniroma3.projectsiw.validator;

import java.text.SimpleDateFormat;

import it.uniroma3.projectsiw.request.AuthorRequest;
import it.uniroma3.projectsiw.response.JSONResponse;

public class AuthorValidator {

	public static boolean isValid(AuthorRequest ar, JSONResponse response){
		boolean result = true;

		if(ar.getName()==null || ar.getName().equals("")){
			response.addError("authorName", "Nome obbligatorio");
			result = false;
		}
		if(ar.getSurname()==null || ar.getSurname().equals("")){
			response.addError("authorSurname", "Cognome obbligatorio");
			result = false;
		}
		if(ar.getNationality() == null || ar.getNationality().equals("")){
			response.addError("authorNationality", "Nazionalit� obbligatoria");
			result = false;
		}
		if(ar.getBirthday() == null){
			response.addError("authorBirthday", "Data di nascita obbligatoria");
			result = false;
		}
		else{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			try{
				sdf.format(ar.getBirthday());
			}catch(Exception e){
				response.addError("authorBirthday", "La data non � nel formato corretto");
				result = false;
			}
		}
		if(ar.getDeathDate() != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			try{
				sdf.format(ar.getBirthday());
			}catch(Exception e){
				response.addError("authorDeathDate", "La data non � nel formato corretto");
				result = false;
			}
		}

		return result;
	}
}
