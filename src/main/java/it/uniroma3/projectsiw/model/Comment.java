package it.uniroma3.projectsiw.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import it.uniroma3.projectsiw.helper.Mapper;
import it.uniroma3.projectsiw.helper.View;
import it.uniroma3.projectsiw.helper.View.ManagedComment;

@Entity
public class Comment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	private Utente utente;

	@ManyToOne
	@JsonIgnore
	private Opera opera;
	
	@Column
	private String comment;
	
	@Column
	private Date date;
	
	public Comment(){
		
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public Opera getOpera() {
		return opera;
	}

	public void setOpera(Opera opera) {
		opera.addComment(this);
		this.opera = opera;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public JSONObject toJson(Class view) {
		// TODO Auto-generated method stub
		return new JSONObject(Mapper.toJson(this, view));
	}
	public static JSONArray toJsonArray(List<Comment> commenti,Class view){
		return new JSONArray(Mapper.toJson(commenti, view));
	}
}
