package it.uniroma3.projectsiw.service;

import java.util.List;

public interface BaseService <T>{
	public T save(T model);
	public List<T> findAll();
	public T findByKey(int id);
	public void remove(T model);
	public int removeAll();
}
