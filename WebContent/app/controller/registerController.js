var app = angular.module("app");

app.controller("registerController", function($scope,$rootScope,$compile,callreq,mainServices,toastr){
	
	//Variable initialization
	$scope.modelRegister = {};
	
	//baseUrl from mainServices
	var baseUrl = mainServices.getBaseUrl();

	$scope.register = function(){

		callreq.callReqPost(baseUrl+"register", $scope.modelRegister,
				function (data)
				{	
					if(data.status == "SUCCESS"){
						location.href="#!/slidshow";
						$rootScope.currentUser = data.user;
						toastr.info('Ciao '+data.user.username+" effettua l'accesso ad Art Gallery");
					}else if(data.status == "REGISTERERROR"){
						toastr.error('Le password non coincidono');
					}else if(data.status == "USERNAMEALREADYEXIST"){
						toastr.error('Username occupato');
					}
				}, 
				function (error)
				{
					console.log(error);
				});
	}
	
	$scope.goToLogin = function(){
		
		location.href = "#!/"
	}
	
});