package it.uniroma3.projectsiw.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import it.uniroma3.projectsiw.helper.HibernateHelper;
import it.uniroma3.projectsiw.model.Author;
import it.uniroma3.projectsiw.model.Comment;
import it.uniroma3.projectsiw.model.Opera;

public class AuthorDao implements BasicDao<Author, Integer>{
 
	@SuppressWarnings("finally")
	public Integer insert(Author model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Integer id = 0;
		try{
			id = (Integer) session.save(model);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return id;
	}

	public boolean delete(Integer id){
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			Opera.deleteByAuthor(id);
			String hql = "delete Author where id =:i";
			Query query = session.createQuery(hql);
			query.setParameter("i", id);
			result = query.executeUpdate() == 1;
		}catch(Exception e){
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public List<Author> findAll() {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		List<Author> author = new ArrayList<Author>();
		try{
			String hql = "from Author";
			Query query = session.createQuery(hql); 
			author = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return author;
	}

	public boolean update(Author model) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		boolean result = false;
		try{
			session.update(model);
			result = true;
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return result;
	}

	public Author findById(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		Author author = null;
		try{
			String hql = "from Author where id = :id";
			Query query = session.createQuery(hql); 
			query.setParameter("id", id);
			author = (Author) query.getSingleResult();
		}catch(Exception e){
		}finally {
			tx.commit();
			session.close();
		}
		return author;
	}

	public List<Opera> findAllOperas(Integer id) {
		Session session = HibernateHelper.startTransaction();
		Transaction tx = session.beginTransaction();
		List<Opera> opera = new ArrayList<Opera>();
		try{
			String hql = "from Opera where author_id =:id";
			Query query = session.createQuery(hql); 
			query.setParameter("id", id);
			opera = query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			tx.commit();
			session.close();
		}
		return opera;
	}

}
