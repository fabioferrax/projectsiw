package it.uniroma3.projectsiw.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import it.uniroma3.projectsiw.model.Opera;

public class OperaCrudRepositoryJPA implements CrudRepositoryInterface<Opera>{
	private EntityManager em;
	private EntityTransaction tx;
	
	public OperaCrudRepositoryJPA(EntityManager em) {
		this.em=em;
	}
	@Override
	public Opera save(Opera model) {
		if(model.getId()==null)
			em.persist(model);
		else
			return  em.merge(model);  //update cos� l'oggetto di ritorno � managed
		return model;
	}

	@Override
	public Opera findByKey(int id) {
		return em.find(Opera.class, id);
	}

	@Override
	public List<Opera> findAll() {
		Query query = em.createQuery("from Opera");
		return query.getResultList();
	}

	@Override
	public void delete(Opera model) {
		em.remove(model);
	}

	@Override
	public int deleteAll() {
		Query query = em.createQuery("delete from Opera");
		return query.executeUpdate();
	}

}
